import pickle
import numpy
import random
import copy
import sys
import time
import theano
import unicodedata

reload(sys)
sys.setdefaultencoding("utf-8")

vecs = numpy.load('glove.embeddings')

voca = pickle.load(open('voca','rb'))
revVoca = pickle.load(open('revVoca','rb'))

gRevVoca = pickle.load(open('revWordG','rb'))

def stripAccent(text):
    text = unicode(text).decode('utf-8')
    return str(''.join(char for char in
                   unicodedata.normalize('NFKD', text)
                   if unicodedata.category(char) != 'Mn'))


def compile_cos_sim_theano():
    v1 = theano.tensor.vector(dtype=theano.config.floatX)
    v2 = theano.tensor.vector(dtype=theano.config.floatX)

    numerator = theano.tensor.sum(v1*v2)
    denominator = theano.tensor.sqrt(theano.tensor.sum(v1**2)*theano.tensor.sum(v2**2))
    return theano.function(inputs = [v1, v2], outputs = numerator/denominator, allow_input_downcast=True)

def calVec(input, mode):
    tempVecs = numpy.zeros((300))
    vCount = 0
#    print(input)
    if mode == 'aver':
        for qTerm in input:
            if qTerm == 'unk':
                tempVecs += vecs[gRevVoca['unk']].astype(numpy.float32)
            else:
                oriTerm = voca[qTerm]
                if oriTerm not in gRevVoca:
                    w2vIndex = gRevVoca['unk']
                else:
                    w2vIndex = gRevVoca[oriTerm]
                tempVecs += vecs[w2vIndex].astype(numpy.float32)
            vCount += 1
        if vCount == 0 : return tempVecs
#        print(tempVecs)
#        print(numpy.mean(tempVecs))
        return tempVecs / vCount
    elif mode == 'foot':
#        print(vecs[input[-1]])
        return vecs[input[-1]].astype(numpy.float)
    elif mode == 'head':
        return vecs[input[0]].astype(numpy.float)

question = pickle.load(open('train','rb'))
answer = pickle.load(open('answers','rb'))

targets = ['who', 'when', 'where']

nerTerms = pickle.load(open('nerTerms','rb'))

vecMode = 'aver'

cosine = compile_cos_sim_theano()

for target in targets:
    numWho = revVoca[target]

    distResult = []
    nerList = {}

    qTerms = list(nerTerms[target])
    qTerms = list(set(qTerms))

    print(len(qTerms))
    print(len(qTerms) * len(question))

    cou = 0

    cosDists = []

    for q in question:
        if numWho in q['question']:
            cou += 1
  
            if cou % 10 == 0 :
                print(str(cou))
                print(time.ctime())
            qVecs = []
            aVecs = []

            if qTerms != []:
                for qTerm in qTerms:
                #print(rawCandi)
                    ansCandi = []

                    if qTerm not in nerList:
                        nerList[qTerm] = []

                    for term in qTerm:
                        term = stripAccent(term).lower()
                        if term not in revVoca:
                            ansCandi.append('unk')
                    #print(revVoca[term])
                        else:
                            ansCandi.append(revVoca[term])

                #print(ansCandi)

                    if ansCandi == []:
                        continue

                    qSent = copy.copy(q['question'][1:])
                    qSent = ansCandi + qSent

#                        print(qSent)

                    qVec = calVec(qSent, vecMode)

                    for aIndex in q['good']:
                        aSent = answer[aIndex]['text']

#                        print(aSent)

                    aVec = calVec(aSent, vecMode)
                    nan = numpy.zeros((300))

                    if qVec.all() == nan.all() or aVec.all() == nan.all():
                        continue

                    nerList[qTerm].append(cosine(qVec, aVec))

        else:
            continue

    for key in nerList.keys() :
        nerList[key] = numpy.mean(nerList[key])
        if numpy.isnan(nerList[key]): del nerList[key]

    min = 0
    minDis = 1

    c = 0

    writer = open('resultDep', 'a')

    for k, v in sorted(nerList.items(), key=lambda x:x[1]) :
        c += 1
        writer.write(target + ' result ' + str(c) + '\n')
        writer.write(str(k) + '\n')
        writer.write(str(v) + '\n')
        if c==20 : break

    writer.close()


