import time
import pickle
import re
from scipy.spatial import distance
import numpy
import random
import copy
from pycorenlp import StanfordCoreNLP
from xml.etree import ElementTree as etree
import sys
import unicodedata
import cudamat as cm

reload(sys)
sys.setdefaultencoding("utf-8")

nlp = StanfordCoreNLP('http://localhost:9000')

vecs = numpy.load('glove.embeddings')
voca = pickle.load(open('/media/wailoktam/wikiqa/wordG','rb'))
revVoca = pickle.load(open('/media/wailoktam/wikiqa/revWordG','rb'))

patWhword = re.compile(ur"W.*", re.DOTALL)
patVerb = re.compile(ur"[M|V].*", re.DOTALL)
patNoun = re.compile(ur"N.*", re.DOTALL)
dbg = open("debug", "w")

def revert(vocab, indices):
    return [vocab.get(i, 'X') for i in indices]

def cosine(a,b):
    a = cm.CUDAMatrix(a)
    b = cm.CUDAMatrix(b)

    c = 1 - cm.vdot(a,b) / (a.euclid_norm() * b.euclid_norm())
    return c

def stripAccent(text):
    text = unicode(text).decode('utf-8')
    return str(''.join(char for char in
                   unicodedata.normalize('NFKD', text)
                   if unicodedata.category(char) != 'Mn'))




def checkTrue(questionIndices,qType):
    whFound = 0
    whJustFound = 0
    polarity = False
    questionText = " ".join(revert(voca, questionIndices))
    output = nlp.annotate(questionText.lower(), properties={
        'annotators': 'tokenize,ssplit,ner,pos',
        'outputFormat': 'xml',
        'timeout': 30000})

    fixed = []
    for o in output:
        fixed.append(o)
    parseXml = etree.fromstring("".join(fixed))
    tokens = parseXml.findall(".//token")

    for t in tokens:


        word = t.find(".//word").text
        pos = t.find(".//POS").text
        print "word"
        print word
        print "pos"
        print pos
        print "qType"
        print qType
        print patWhword.match(pos)
        print patVerb.match(pos)

        if whFound == 0 and patWhword.match(pos) and word == qType:
            whFound = 1
            whJustFound = 1

        print "whJustFound"
        print whJustFound

        if whJustFound == 1 and not patWhword.match(pos):
            whJustFound = 0
            print "hitOut"
            if qType == "what" and patVerb.match(pos):
                print "hitIn"
                polarity = True
                break
            if qType == "how" and patVerb.match(pos):
                print "hitIn"
                polarity = True
                break

#    print qType
#    print questionText
#    print polarity
    return polarity

def calVec(input, mode):
    tempVecs = []
#    print(input)
    if mode == 'aver':
        count = 0
        tempVecs = numpy.zeros((300,1))
        if type(input) != list:
            return vecs[input].astype(numpy.float32).reshape((300,1))

        for term in input:
            tempVecs += vecs[term].astype(numpy.float32).reshape((300,1))
            count += 1
#        print(tempVecs)
        if count == 0 : return numpy.zeros((300,1))
        return tempVecs / count
    elif mode == 'foot':
#        print(vecs[input[-1]])
        return vecs[input[-1]].astype(numpy.float)
    elif mode == 'head':
        return vecs[input[0]].astype(numpy.float)


def lparse(input):

    output = nlp.annotate(input.lower(), properties={
        'annotators': 'tokenize,ssplit,pos',
        'outputFormat': 'xml',
        'timeout': 30000})
    fixed = []
    for o in output:
        fixed.append(o)
    parseXml = etree.fromstring("".join(fixed))
    tokens = parseXml.findall(".//token")

    sent = []
    for token in tokens:
        word = token.find(".//word").text.lower()
        if word not in revVoca:
            sent.append(revVoca['unk'])
        else:
            sent.append(revVoca[word])
    return sent

file = open('/media/wailoktam/wikiqa/WikiQASent.pos.ans.tsv', 'r')
lines = file.readlines()

#targets = ['who', 'when', 'where'] # 'when' 'what' 'who' 'where'
targets = ['how', 'what']

for target in targets:

    tarNum = len(target)

    question = []
    answer = []

    numWho = revVoca[target]

    aCount = 0

    numWhat = 0

    for line in lines:
    #    print line
        if line.split('\t')[1][0:tarNum].lower() != target:
            continue

        qData = {}
        qSent = lparse(stripAccent(line.split('\t')[1]))

        if qSent == []:
            continue

        aTemp = []
        for a in line.split('\t')[6:]:
            if a.strip() == '':
                continue
            aSent = lparse(stripAccent(a))
            if aSent == []:
                continue
            aData = {}
            aData['id'] = aCount
            aCount += 1
            aData['text'] = aSent
            aTemp.append(aData)

        qGood = []
        for temp in aTemp:
            qGood.append(temp['id'])
        if qGood == []:
            continue

        qData['question'] = qSent
        qData['good'] = qGood

        if numWho in qSent:
            numWhat += 1

        question.append(qData)
        answer.extend(aTemp)

    numWho = revVoca[target]

    vecMode = 'aver' # head, foot, aver

#    print('vec mode : ' + vecMode)

    baseLine = 1.0

    nerTerms = pickle.load(open('/media/wailoktam/wikiqa/nerTerms','rb'))

    distResult = []
    nerList = {}

    qTerms = list(nerTerms[target])
#    print qTerms
    qTerms = list(set(qTerms))

#    print(len(qTerms))
#    print(len(qTerms) * len(question))

    cou = 0

    cosDists = []

    for q in question:
        if numWho in q['question']:
            cou += 1
  
#            if cou % 10 == 0 :
#                print(str(cou) + '/' + str(numWhat))
#                print(time.ctime())
            qVecs = []
            aVecs = []

            if qTerms != []:
                for qTerm in qTerms:
                #print(rawCandi)
                    ansCandi = []

                    if qTerm not in nerList:
                        nerList[qTerm] = []

                    for term in qTerm:
                        
                        term = stripAccent(term).lower()
                        if term not in revVoca:
                            continue
                        print(revVoca[term])
                        ansCandi.append(revVoca[term])

                #print(ansCandi)

                    if not checkTrue(q['question'],target):
                        continue

                    if ansCandi == []:
                        continue

                    qSent = copy.copy(q['question'][1:])
                    qSent = ansCandi + qSent
                    print "ansCandi"
                    print ansCandi
                    print revert(voca,ansCandi)
                    print "qSent"
                    print qSent
                    print revert(voca, qSent)

                    dbg.write(" ".join(qSent))

                    qVec = calVec(qSent, vecMode)

                    for aIndex in q['good']:
                        aSent = answer[aIndex]['text']

#                        print(aSent)

                    aVec = calVec(aSent, vecMode)
                    nan = numpy.zeros((300))

                    if qVec.all() == nan.all() or aVec.all() == nan.all():
                        continue

                    nerList[qTerm].append(cosine(qVec, aVec))

        else:
            continue

    for key in nerList.keys() :
        nerList[key] = numpy.mean(nerList[key])
        if numpy.isnan(nerList[key]): del nerList[key]

    min = 0
    minDis = 1

    c = 0

    writer = open('resultGlove', 'a')

    for k, v in sorted(nerList.items(), key=lambda x:x[1]) :
        c += 1
        writer.write(target + ' result ' + str(c) + '\n')
        writer.write(str(k) + '\n')
        writer.write(str(v) + '\n')
        if c==20 : break

    writer.close()
    dbg.close()
