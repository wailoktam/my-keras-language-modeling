from __future__ import print_function

import unicodedata

import numpy


import pickle
import sys
reload(sys)
sys.setdefaultencoding("utf8")


def stripAccent(text):
    text = unicode(text).decode('utf-8')
    return str(''.join(char for char in unicodedata.normalize('NFKD', text) if unicodedata.category(char) != 'Mn'))


##### Converting / reverting #####

def convert(rvocab, words):
    if type(words) == str:
        words = words.strip().lower().split(' ')
    return [rvocab.get(w, 0) for w in words]


def revert(vocab, indices):
    return [(vocab.get(i, 'X')) for i in indices]

    ##### Padding #####


def regen(dataList, oldVocab, newRevVocab, debugFile):
    newDataList = []
    for line in dataList:
        oldInd = line["question"]
        debugRegen.write("oldQ\n")
        debugRegen.write(u" ".join(stripAccent(revert(oldVocab,oldInd))).encode('utf-8'))
        debugRegen.write("\n")
        newLine = {}
        newInd = convert(newRevVocab, u" ".join(revert(oldVocab, oldInd)).encode('utf-8'))
        newLine["question_id"] = line["question_id"]
        newLine["question"] = newInd
        debugRegen.write("newQ\n")
        debugFile.write(u" ".join(stripAccent(revert(newVocab, newInd))))
        debugFile.write("\n")
        newLine["good"] = line["good"]
        newLine["bad"] = line["bad"]
        newDataList.append(newLine)
    return newDataList


    ##### Evaluation #####



if __name__ == '__main__':
    oldTrain = pickle.load(open("/media/wailoktam/wikiqa/trainQTCt","rb"))
    oldDev = pickle.load(open("/media/wailoktam/wikiqa/devQTCt","rb"))
    oldTest = pickle.load(open("/media/wailoktam/wikiqa/testQTCt","rb"))
    oldVocab = pickle.load(open("/media/wailoktam/wikiqa/wordQT","rb"))
    oldRevVocab = pickle.load(open("/media/wailoktam/wikiqa/revWordQT","rb"))
    newVocab = pickle.load(open("/media/wailoktam/wikiqa/wordQTd", "rb"))
    newRevVocab = pickle.load(open("/media/wailoktam/wikiqa/revWordQTd", "rb"))
    debugRegen = open("debugRegen","w")







    newTrain = regen(oldTrain,oldVocab,newRevVocab, debugRegen)
    newDev = regen(oldDev, oldVocab, newRevVocab, debugRegen)
    newTest = regen(oldTest, oldVocab, newRevVocab, debugRegen)



    newTrainFile = open("/media/wailoktam/wikiqa/trainQTCtd", "wb")
    pickle.dump(newTrain, newTrainFile)
    newTrainFile.close()

    newDevFile = open("/media/wailoktam/wikiqa/devQTCtd", "wb")
    pickle.dump(newDev, newDevFile)
    newDevFile.close()

    newTestFile = open("/media/wailoktam/wikiqa/testQTCtd", "wb")
    pickle.dump(newTest, newTestFile)
    newTestFile.close()



    debugRegen.close()
