from __future__ import print_function



import unicodedata
import numpy


import pickle
import sys
reload(sys)
sys.setdefaultencoding("utf8")


def stripAccent(text):
    text = unicode(text).decode('utf-8')
    return str(''.join(char for char in unicodedata.normalize('NFKD', text) if unicodedata.category(char) != 'Mn'))


##### Converting / reverting #####

def convert(rvocab, words):
    if type(words) == str:
        words = words.strip().lower().split(' ')
    return [rvocab.get(w, rvocab["unk"]) for w in words]


def revert(vocab, indices):
    return [(vocab.get(i, 'X')) for i in indices]

    ##### Padding #####


def regen(dataList, oldVocab, newRevVocab, debugFile):
    newDataList = []
    for line in dataList:
        oldInd = line["question"]
        debugRegen.write("oldQ\n")
        debugRegen.write(stripAccent(" ".join(revert(oldVocab,oldInd))))
        debugRegen.write("\n")
        newLine = {}
        newInd = convert(newRevVocab, stripAccent(" ".join(revert(oldVocab, oldInd))))
        newLine["question_id"] = line["question_id"]
        newLine["question"] = newInd
        debugRegen.write("newQ\n")
        debugFile.write(stripAccent(" ".join(revert(newVocab, newInd))))
        debugFile.write("\n")
        newLine["good"] = line["good"]
        newLine["bad"] = line["bad"]
        newDataList.append(newLine)
    return newDataList


    ##### Evaluation #####



if __name__ == '__main__':
    oldAns = pickle.load(open("/media/wailoktam/trecqa/answers", "rb"))
    oldTrain = pickle.load(open("/media/wailoktam/trecqa/trainQTCt_oth","rb"))
    oldDev = pickle.load(open("/media/wailoktam/trecqa/devQTCt_oth","rb"))
    oldTest = pickle.load(open("/media/wailoktam/trecqa/testQTCt_oth","rb"))
    oldVocab = pickle.load(open("/media/wailoktam/trecqa/voca","rb"))
    oldRevVocab = pickle.load(open("/media/wailoktam/trecqa/revVoca","rb"))
    newVocab = pickle.load(open("/media/wailoktam/trecqa/wordG", "rb"))
    newRevVocab = pickle.load(open("/media/wailoktam/trecqa/revWordG", "rb"))
    newAns = []
    debugRegen = open("debugRegen","w")

    for line in oldAns:
        oldInd = line["text"]
        newLine = {}
        debugRegen.write("oldAns\n")
        #        debugRegen.write(u" ".join(revert(oldVocab,oldInd)).encode('utf-8'))
        debugRegen.write(stripAccent(" ".join(revert(oldVocab, oldInd))))
        debugRegen.write("\n")
        newInd = convert(newRevVocab, stripAccent(" ".join(revert(oldVocab, oldInd))))
        #        newInd = convert(newRevVocab, u" ".join(revert(oldVocab,oldInd)).encode('utf-8'))
        debugRegen.write("newAns\n")
        #        debugRegen.write(u" ".join(revert(newVocab, newInd)).encode('utf-8'))
        debugRegen.write(stripAccent(" ".join(revert(newVocab, newInd))))
        debugRegen.write("\n")
        newLine["id"] = line["id"]
        newLine["text"] = newInd
        newAns.append(newLine)

    newTrain = regen(oldTrain,oldVocab,newRevVocab, debugRegen)
    newDev = regen(oldDev, oldVocab, newRevVocab, debugRegen)
    newTest = regen(oldTest, oldVocab, newRevVocab, debugRegen)

    newAnsFile = open("/media/wailoktam/trecqa/answersG", "wb")
    pickle.dump(newAns, newAnsFile)
    newAnsFile.close()

    newTrainFile = open("/media/wailoktam/trecqa/trainQTCtg_oth", "wb")
    pickle.dump(newTrain, newTrainFile)
    newTrainFile.close()

    newDevFile = open("/media/wailoktam/trecqa/devQTCtg_oth", "wb")
    pickle.dump(newDev, newDevFile)
    newDevFile.close()

    newTestFile = open("/media/wailoktam/trecqa/testQTCtg_oth", "wb")
    pickle.dump(newTest, newTestFile)
    newTestFile.close()



    debugRegen.close()
