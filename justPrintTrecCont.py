from __future__ import print_function


#os.environ['TREC_QA'] = '../trecqa'

import numpy


import pickle
import itertools
import unicodedata

import sys
reload(sys)
sys.setdefaultencoding("utf8")


def stripAccent(text):
    text = unicode(text).decode('utf-8')
    return str(''.join(char for char in unicodedata.normalize('NFKD', text) if unicodedata.category(char) != 'Mn'))




    ##### Converting / reverting #####

def convert(rvocab, words):
    if type(words) == str:
        words = words.strip().lower().split(' ')
    return [rvocab.get(w, rvocab["unk"]) for w in words]

def revert(vocab, indices):
    return [vocab.get(i, 'X') for i in indices]

    ##### Padding #####


def printCont(dataList, vocab, debugFile):
    for line in dataList:
        indices = line["question"]
        debugFile.write(stripAccent(" ".join(revert(vocab, indices))))
        debugFile.write("\n")



    ##### Evaluation #####



if __name__ == '__main__':
    newAns = pickle.load(open("/media/wailoktam/trecqa/answersG", "rb"))
    newTrainOth = pickle.load(open("/media/wailoktam/trecqa/trainQTCtg_oth", "rb"))
    newDevOth = pickle.load(open("/media/wailoktam/trecqa/devQTCtg_oth", "rb"))
    newTestOth = pickle.load(open("/media/wailoktam/trecqa/testQTCtg_oth", "rb"))

    newVocab = pickle.load(open("/media/wailoktam/trecqa/wordG", "rb"))
    newRevVocab = pickle.load(open("/media/wailoktam/trecqa/revWordG", "rb"))
    debugRegen = open("debugRegen","w")

    printCont(newTestOth, newVocab, debugRegen)




    debugRegen.close()
