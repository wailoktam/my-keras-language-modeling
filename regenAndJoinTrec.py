from __future__ import print_function


#os.environ['TREC_QA'] = '../trecqa'

import numpy


import pickle
import itertools
import unicodedata

import sys
reload(sys)
sys.setdefaultencoding("utf8")


def stripAccent(text):
    text = unicode(text).decode('utf-8')
    return str(''.join(char for char in unicodedata.normalize('NFKD', text) if unicodedata.category(char) != 'Mn'))




    ##### Converting / reverting #####

def convert(rvocab, words):
    if type(words) == str:
        words = words.strip().lower().split(' ')
    return [rvocab.get(w, rvocab["unk"]) for w in words]

def revert(vocab, indices):
    return [vocab.get(i, 'X') for i in indices]

    ##### Padding #####


def regen(dataList, oldVocab, newRevVocab, debugFile):
     newDataList = []
     for line in dataList:
        oldInd = line["question"]
        debugRegen.write("oldQ\n")
        debugRegen.write(stripAccent(" ".join(revert(oldVocab, oldInd))))
        debugRegen.write("\n")
        newLine = {}
        newInd = convert(newRevVocab, stripAccent(" ".join(revert(oldVocab, oldInd))))
        newLine["question_id"] = line["question_id"]
        newLine["question"] = newInd
        debugRegen.write("newQ\n")
        debugFile.write(stripAccent(" ".join(revert(newVocab, newInd))))
        debugFile.write("\n")
        newLine["good"] = line["good"]
        newLine["bad"] = line["bad"]
        newDataList.append(newLine)
     return newDataList

    ##### Evaluation #####



if __name__ == '__main__':
    oldAns = pickle.load(open("/media/wailoktam/trecqa/answers", "rb"))
    oldTrainWho = pickle.load(open("/media/wailoktam/trecqa/trainQT_who","rb"))
    oldDevWho = pickle.load(open("/media/wailoktam/trecqa/devQT_who","rb"))
    oldTestWho = pickle.load(open("/media/wailoktam/trecqa/testQT_who","rb"))
    oldTrainWhen = pickle.load(open("/media/wailoktam/trecqa/trainQT_when", "rb"))
    oldDevWhen = pickle.load(open("/media/wailoktam/trecqa/devQT_when", "rb"))
    oldTestWhen = pickle.load(open("/media/wailoktam/trecqa/testQT_when", "rb"))
    oldTrainWhere = pickle.load(open("/media/wailoktam/trecqa/trainQT_where", "rb"))
    oldDevWhere = pickle.load(open("/media/wailoktam/trecqa/devQT_where", "rb"))
    oldTestWhere = pickle.load(open("/media/wailoktam/trecqa/testQT_where", "rb"))
    oldTrainOth = pickle.load(open("/media/wailoktam/trecqa/trainQTCt_oth", "rb"))
    oldDevOth = pickle.load(open("/media/wailoktam/trecqa/devQTCt_oth", "rb"))
    oldTestOth = pickle.load(open("/media/wailoktam/trecqa/testQTCt_oth", "rb"))
    oldVocab = pickle.load(open("/media/wailoktam/trecqa/voca","rb"))
    oldRevVocab = pickle.load(open("/media/wailoktam/trecqa/revVoca","rb"))
    newVocab = pickle.load(open("/media/wailoktam/trecqa/wordG", "rb"))
    newRevVocab = pickle.load(open("/media/wailoktam/trecqa/revWordG", "rb"))
    debugRegen = open("debugRegen","w")

    newAns = []
#    newVocabCount = 1
#    newVocabFile = "wordQTd"
#    newEmbedFile = open("deps.words","r")


    for line in oldAns:
        oldInd = line["text"]
        newLine = {}
        debugRegen.write("oldAns\n")
        #        debugRegen.write(u" ".join(revert(oldVocab,oldInd)).encode('utf-8'))
        debugRegen.write(stripAccent(" ".join(revert(oldVocab, oldInd))))
        debugRegen.write("\n")
        newInd = convert(newRevVocab, stripAccent(" ".join(revert(oldVocab, oldInd))))
        #        newInd = convert(newRevVocab, u" ".join(revert(oldVocab,oldInd)).encode('utf-8'))
        debugRegen.write("newAns\n")
        #        debugRegen.write(u" ".join(revert(newVocab, newInd)).encode('utf-8'))
        debugRegen.write(stripAccent(" ".join(revert(newVocab, newInd))))
        debugRegen.write("\n")
        newLine["id"] = line["id"]
        newLine["text"] = newInd
        newAns.append(newLine)

    newTrainWho =regen(oldTrainWho,oldVocab,newRevVocab, debugRegen)
    newDevWho = regen(oldDevWho, oldVocab, newRevVocab, debugRegen)
    newTestWho = regen(oldTestWho, oldVocab, newRevVocab, debugRegen)
    newTrainWhen =regen(oldTrainWhen,oldVocab,newRevVocab, debugRegen)
    newDevWhen = regen(oldDevWhen, oldVocab, newRevVocab, debugRegen)
    newTestWhen = regen(oldTestWhen, oldVocab, newRevVocab, debugRegen)
    newTrainWhere =regen(oldTrainWhere,oldVocab,newRevVocab, debugRegen)
    newDevWhere = regen(oldDevWhere, oldVocab, newRevVocab, debugRegen)
    newTestWhere = regen(oldTestWhere, oldVocab, newRevVocab, debugRegen)
    newTrainOth =regen(oldTrainOth,oldVocab,newRevVocab, debugRegen)
    newDevOth = regen(oldDevOth, oldVocab, newRevVocab, debugRegen)
    newTestOth = regen(oldTestOth, oldVocab, newRevVocab, debugRegen)


    newTrain = list(itertools.chain(newTrainOth,newTrainWho,newTrainWhen,newTrainWhere))
    newDev = list(itertools.chain(newDevOth, newDevWho, newDevWhen, newDevWhere))
    newTest = list(itertools.chain(newTestOth, newTestWho, newTestWhen, newTestWhere))

    newAnsFile = open("/media/wailoktam/trecqa/answersG", "wb")
    pickle.dump(newAns, newAnsFile)
    newAnsFile.close()


    newTrainFile = open("/media/wailoktam/trecqa/trainMixg", "wb")
    pickle.dump(newTrain, newTrainFile)
    newTrainFile.close()

    newDevFile = open("/media/wailoktam/trecqa/devMixg", "wb")
    pickle.dump(newDev, newDevFile)
    newDevFile.close()

    newTestFile = open("/media/wailoktam/trecqa/testMixg", "wb")
    pickle.dump(newTest, newTestFile)
    newTestFile.close()

    debugRegen.close()
