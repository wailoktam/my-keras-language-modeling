# Finding Prototypes of Answers for Improving Answer Sentence Selection 

This repository is created mainly for storing scripts used for training and evaluating with data transformed by our technique covered in Tam et al.: Finding Prototypes of Answers for Improving Answer Sentence Selection, IJCAI 2017.One of the baseline systems on which the transformed data are trained and evaluated is a third-party reimplementation of Tan et al.: LSTM-Based Deep Learning Models for Nonfactoid Answer Selection, eprint arXiv:1511.04108. The reimplementation can be found in [https://github.com/codekansas/keras-language-modeling](https://github.com/codekansas/keras-language-modeling). We have made some slight modification on this reimplementation:     


* `trec_qa_eval.py` is created from insurance_qa_eval.py for handling TrecQA data. `wiki_qa_eval.py` is created from insurance_qa_eval.py for handling WikiQA data.
question_len and answer_len in `trec_qa_eval.py` and `wiki_qa_eval.py` are changed to cater to the needs of TrecQA and WikiQA data. 
 
* making sure that answers selected randomly from the answer pool as bad answers for a question would not be a good answer
 
* including computation of mean average precision during evaluation in `trec_qa_eval.py` and `wiki_qa_eval.py`

## Purposes of scripts written from scratch

### Generate embeddings for datasets

* generate_insurance_qa_embeddings.py
* generate_insurance_qa_V2_embeddings.py
* generate_trec_qa_embeddings.py
* generate_wiki_qa_embeddings.py

### Regenerate untransformed data with new embeddings

* regenTrainingFrNewEmbedding.py
* regenTrainQuickCt.py
* regenTrainQuickCtg.py
* regenTrainTrecQTCt.py

### Regenerate transformed data with new embeddings

* regenTrainQuick.py
* regenTrainQuick.py
* regenTrainTrecQT.py

### Mixing transformed data of targeted question types and untransformed data of non-targets

* regenAndJoinTrec.py
* regenAndJoinWiki.py