import pickle
from scipy.spatial import distance
import numpy
import random
import copy
from pycorenlp import StanfordCoreNLP
from xml.etree import ElementTree as etree
import sys
import unicodedata

reload(sys)
sys.setdefaultencoding("utf-8")

nlp = StanfordCoreNLP('http://localhost:9000')

vecs = numpy.load('glove.embeddings.added0')
voca = pickle.load(open('/media/wailoktam/wikiqa/wordG','rb'))
revVoca = pickle.load(open('/media/wailoktam/wikiqa/revWordG','rb'))

def stripAccent(text):
    text = unicode(text).decode('utf-8')
    return str(''.join(char for char in
                   unicodedata.normalize('NFKD', text)
                   if unicodedata.category(char) != 'Mn'))

def calVec(input, mode):
    tempVecs = []
#    print(input)
    if mode == 'aver':
        count = 0
        tempVecs = numpy.zeros((300))
        if type(input) != list:
            if input not in revVoca:
                index = revVoca['unk']
            else:
                index = revVoca[input]
            return vecs[index].astype(numpy.float32)

        for term in input:
            if term not in revVoca:
#                print 'not term in w2vRevVoca : ' + str(term)
#                errorTerm.add(term)
#                continue
                index = revVoca['unk']
            else:
                index = revVoca[term]

            tempVecs += vecs[index].astype(numpy.float32)
            count += 1
#        print(tempVecs)
        if count == 0 : return numpy.zeros((300))
        return tempVecs / count
    elif mode == 'foot':
#        print(vecs[input[-1]])
        return vecs[input[-1]].astype(numpy.float)
    elif mode == 'head':
        return vecs[input[0]].astype(numpy.float)


def lparse(input):
    dbg = open("debug","w")
    output = nlp.annotate(input.lower(), properties={
        'annotators': 'tokenize,ssplit,pos',
        'outputFormat': 'xml',
        'timeout': 30000})
    fixed = []
    for o in output:
        fixed.append(o)
    parseXml = etree.fromstring("".join(fixed))
    tokens = parseXml.findall(".//token")

    sent = []
    for token in tokens:
        word = token.find(".//word").text
        sent.append(word)        
    return sent

file = open('/media/wailoktam/wikiqa/WikiQASent.pos.ans.tsv', 'r')
lines = file.readlines()

question = []

qTypes = ['who', 'when', 'where'] #['who', 'what', 'when', 'where']
qTerms = {}


for line in lines:

    for qType in qTypes:

        if qType not in qTerms:
            qTerms[qType] = []

        if qType not in line.split('\t')[1].lower():
            continue

        qDatas = line.split('\t')[7:]

        qTemp = ''
        qLen = 11
        for q in qDatas:
            if qLen > len(q.split(' ')) and q != 'NO_ANS':
                qLen = len(q.split(' '))
                qTemp = q

        if qLen == 11:
            continue
        if qTemp.strip() == '':
            continue

        qLemmas = lparse(stripAccent(qTemp))
        if qLemmas == []:
            continue
        qTerms[qType].append(qLemmas)

#qTerms['who'].append(revVoca['who'])
#qTerms['when'].append(revVoca['when'])
#qTerms['where'].append(revVoca['where'])
#qTerms['what'].append(revVoca['what'])

nerTerms = pickle.load(open('nerTerms'))
for qType in qTypes:
    nerTerms[qType] = list(set(nerTerms[qType]))

#nerTerms['what'].extend(nerTerms['who'])
#nerTerms['what'].extend(nerTerms['when'])
#nerTerms['what'].extend(nerTerms['where'])
#nerTerms['what'] = list(set(nerTerms['what']))


vecMode = 'aver' # head, foot, aver

print('vec mode : ' + vecMode)

baseLine = 1.0

cosDis = {}



for qType in qTypes:
    print(qType + ' start, # of ' + qType + ' is ' + str(len(qTerms[qType])))

    if qType not in cosDis:
        cosDis[qType] = {}

    count = 0

    for qTerm in qTerms[qType]:

        count += 1
        if count % 10 == 0 : print (count)

        qVec = calVec(qTerm, vecMode)

        for n in nerTerms[qType]:
            if n not in cosDis[qType]:
                cosDis[qType][n] = []
            n = list(n)
            nVec = calVec(n, vecMode)
            if qVec.all() == numpy.zeros((300)).all() or nVec.all() == numpy.zeros((300)).all():
                continue
            cosDis[qType][tuple(n)].append(distance.cosine(qVec, nVec))

for qType in qTypes:

    for key in cosDis[qType].keys() :
#        print key
        cosDis[qType][key] = numpy.mean(cosDis[qType][key])
        if numpy.isnan(cosDis[qType][key]):
            del cosDis[qType][key] 

    c = 0
    for k, v in sorted(cosDis[qType].items(), key=lambda x:x[1]) :
        c += 1
        print(qType + ' result ' + str(c))
        print(k)
        print(v)
        if c==10 : break


