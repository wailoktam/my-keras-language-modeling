import pickle
from scipy.spatial import distance
import numpy
import random
import copy
from pycorenlp import StanfordCoreNLP
from xml.etree import ElementTree as etree
from gensim.models import KeyedVectors

import sys
import unicodedata
import re
reload(sys)
sys.setdefaultencoding("utf-8")

nlp = StanfordCoreNLP('http://localhost:9000')

#vecs = numpy.load('glove.embeddings.added0')
#voca = pickle.load(open('/media/wailoktam/wikiqa/wordG','rb'))
#revVoca = pickle.load(open('/media/wailoktam/wikiqa/revWordG','rb'))

#gensimModel=KeyedVectors.load_word2vec_format("glove_model.txt",binary=False) #

patWhword = re.compile(ur"W.*", re.DOTALL)
patVerb = re.compile(ur"[M|V].*", re.DOTALL)
patNoun = re.compile(ur"N.*", re.DOTALL)
dbg = open("debug", "w")

def stripAccent(text):
    text = unicode(text).decode('utf-8')
    return str(''.join(char for char in
                   unicodedata.normalize('NFKD', text)
                   if unicodedata.category(char) != 'Mn'))


def revert(vocab, indices):
    return [vocab.get(i, 'X') for i in indices]

def parse2Tokens(input):
    output = nlp.annotate(input.lower(), properties={
        'annotators': 'tokenize,ssplit,ner,pos',
        'outputFormat': 'xml',
        'timeout': 30000})

    fixed = []
    for o in output:
        fixed.append(o)
    parseXml = etree.fromstring("".join(fixed))

    tokens = parseXml.findall(".//token")
    return tokens

def getNers(qType,question,answer):


    qTokens = parse2Tokens(question)
    aTokens = parse2Tokens(answer)
#    wordEntPairs = []
    wordList = []
    whFound = 0
    whJustFound = 0
    trueWhat = 0
    trueHow = 0
    for qT in qTokens:
        word = qT.find(".//word").text
        pos = qT.find(".//POS").text



        if whFound ==0 and patWhword.match(pos) and word==qType:
            whFound = 1
            whJustFound = 1

        print "word"
        print word
        print "qType"
        print qType
        print "pos"
        print pos
        print "whJustFound"
        print whJustFound
        if patVerb.match(pos):
            print "hit"
        else:
            print "miss"



        if whJustFound == 1 and not patWhword.match(pos):

            whJustFound = 0

            if qType == "what" and patVerb.match(pos) :

                trueWhat = 1

                break
            if qType == "how" and patVerb.match(pos):
                trueHow = 1

                break
    print "trueWhat"
    print trueWhat
    print "trueHow"
    print trueHow
#    dbg.write("question\n")
#    dbg.write(question+"\n")
##    dbg.write("trueWhat\n")
 #   dbg.write(str(trueWhat)+"\n")
#    dbg.write("trueHow\n")
#    dbg.write(str(trueHow)+"\n")
    prevNer = 0
    for aT in aTokens:
#        wordEntPair = {}

        ner = aT.find(".//NER").text
        word = aT.find(".//word").text
        pos = aT.find(".//POS").text
#        print "word"
#        print word
#        print "qType"
#        print qType
#        print "trueWhat"
#        print trueWhat
#        print "trueHow"
##        print trueHow
 #       print "ner"
 #       print ner


        if trueWhat == 1 and qType == "what" and prevNer == 1 and ner == "O":


    # wordEntPair[word] = ner
    #            print "what case"
    #            print word
    #            print ner
    #            print wordEntPair
    #           wordEntPairs.append(word)
            wordList.append(nerString)

            prevNer = 0

        if trueHow == 1 and qType == "how" and prevNer == 1 and ner == "O":


    # wordEntPair[word] = ner
    #            print "what case"
    #            print word
    #            print ner
    #            print wordEntPair
    #           wordEntPairs.append(word)
            wordList.append(nerString)

            prevNer = 0


        if trueWhat == 1 and qType == "what" and not ner == "O":

            if prevNer == 0:
                nerString = word
#            wordEntPair[word] = ner
#            print "what case"
#            print word
#            print ner
#            print wordEntPair
#            wordEntPairs.append(word)

            else:
                nerString = nerString + " " + word
            prevNer = 1


        if trueHow == 1 and qType == "how" and not ner == "O":
#            wordEntPair[word] = ner
#            print "how case"
#            print word
#            print ner
#            print wordEntPair
#            wordEntPairs.append(word)
            if prevNer == 0:
                nerString = word
            else:
                nerString = nerString + " " + word
            prevNer = 1


    return wordList


def whatParse(question,answer):
    dbg = open("debug","w")
    output = nlp.annotate(question.lower(), properties={
        'annotators': 'tokenize,ssplit,ner,pos',
        'outputFormat': 'xml',
        'timeout': 30000})

    fixed = []
    for o in output:
        fixed.append(o)
    dbg.write("".join(fixed))
    parseXml = etree.fromstring("".join(fixed))

    tokens = parseXml.findall(".//token")
    questionTypePair = {}

    whatFlag = 0
    for token in tokens:

        ner = token.find(".//NER").text
        word = token.find(".//word").text
        pos = token.find(".//POS").text
        if whatFlag == 1:
            sim2Who = gensimModel.similarity(word,"william")
            sim2When = gensimModel.similarity(word,"1923")
            sim2Where = gensimModel.similarity(word, "florida")
            if max(sim2Who,sim2When,sim2Where) == sim2Who:
                questionTypePair[question] = "who"
            elif max(sim2Who, sim2When, sim2Where) == sim2When:
                questionTypePair[question] = "when"
            else:
                questionTypePair[question] = "where"

            whatFlag = 0


        if word == "what": whatFlag = 1
    return questionTypePair


file = open('/media/wailoktam/wikiqa/WikiQASent.pos.ans.tsv', 'r')
lines = file.readlines()

question = []

qTypes = ['how', 'what']

qwordEntDict = {}
questionTypePairs = []







for line in lines:

    for qType in qTypes:
        if qType not in qwordEntDict:
            qwordEntDict[qType] = []

        if qType not in line.split('\t')[1].lower():
            continue


        ners = getNers(qType,stripAccent(line.split('\t')[1]),stripAccent(line.split('\t')[5]))
#        print "entPairs"
#        print entPairs
        qwordEntDict[qType] = qwordEntDict[qType] + ners

with open('/media/wailoktam/wikiqa/nerTerms', mode='wb') as nerFile:
    pickle.dump(qwordEntDict, nerFile)


dbg.close()








