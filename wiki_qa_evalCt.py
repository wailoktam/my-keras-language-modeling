
from __future__ import print_function

import os
#os.environ['TREC_QA'] = '../trecqa'
import sys
import random
import itertools
from time import strftime, gmtime
import unicodedata

import sys
reload(sys)
sys.setdefaultencoding("utf8")
import pickle

from keras.optimizers import Adam
from scipy.stats import rankdata

from keras_models import EmbeddingModel, AttentionModel, ConvolutionModel

random.seed(42)

def stripAccent(text):
    text = unicode(text).decode('utf-8')
    return str(''.join(char for char in unicodedata.normalize('NFKD', text) if unicodedata.category(char) != 'Mn'))

class Evaluator:
    def __init__(self, conf=None):
        try:
            data_path = os.environ['WIKI_QA']
        except KeyError:
            print("WIKI_QA is not set.")
            sys.exit(1)
        self.path = data_path
        self.conf = dict() if conf is None else conf
        self.params = conf.get('training_params', dict())
        ansDict = {}
        ansList = self.load('answersG')
#        ansList = self.load('answersQTd')
#        ansList = self.load('answersQTCtd')
#        ansList = self.load('answersQT')

        for a in ansList:
            #            print (a)
            ansDict[a["id"]] = a["text"]
        self.answers = ansDict # self.load('generated')
        self._vocab = None
        self._reverse_vocab = None
        self._eval_sets = None

    ##### Resources #####

    def load(self, name):
        return pickle.load(open(os.path.join(self.path, name), 'rb'))

    def vocab(self):
        if self._vocab is None:
#            self._vocab = self.load('wordQTd')
            self._vocab = self.load('wordG')
#            self._vocab = self.load('wordQTCt')
#            self._vocab = self.load('wordQT')
        return self._vocab

    def reverse_vocab(self):
        if self._reverse_vocab is None:
            vocab = self.vocab()
            self._reverse_vocab = dict((v.lower(), k) for k, v in vocab.items())
        return self._reverse_vocab

    ##### Loading / saving #####

    def save_epoch(self, model, epoch):
        if not os.path.exists('modelsWikiCt/'):
            os.makedirs('modelsWikiCt/')
        model.save_weights('modelsWikiCt/weights_epoch_%d.h5' % epoch, overwrite=True)

    def load_epoch(self, model, epoch):
        assert os.path.exists('modelsWikiCt/weights_epoch_%d.h5' % epoch), 'Weights at epoch %d not found' % epoch
        model.load_weights('modelsWikiCt/weights_epoch_%d.h5' % epoch)

    ##### Converting / reverting #####

    def convert(self, words):
        rvocab = self.reverse_vocab()
        if type(words) == str:
            words = words.strip().lower().split(' ')
        return [rvocab.get(w, 0) for w in words]

    def revert(self, indices):
        vocab = self.vocab()
        return [vocab.get(i, 'X') for i in indices]

    ##### Padding #####


    def padq(self, data):
        return self.pad(data, self.conf.get('question_len', None))

    def pada(self, data):
        return self.pad(data, self.conf.get('answer_len', None))

    def pad(self, data, len=None):
        from keras.preprocessing.sequence import pad_sequences
        #        print ("data")
        #        print (data)
        return pad_sequences(data, maxlen=len, padding='post', truncating='post', value=2196017)

    ##### Training #####

    def print_time(self):
        print(strftime('%Y-%m-%d %H:%M:%S :: ', gmtime()), end='')

    def train(self, model):
        epoch_count = 0
        debugTrainFile = open("debugTrainWikiCt", "w")
        save_every = self.params.get('save_every', None)
        batch_size = self.params.get('batch_size', 100)
        nb_epoch = self.params.get('nb_epoch', 2001)
        split = self.params.get('validation_split', 0)
#        training_set = self.load('trainQTd')
#        dev_set = self.load('devQTd')
        training_set = self.load('trainG')
        dev_set = self.load('devG')
#        training_set = self.load('trainQTCt')
#        dev_set = self.load('devQTCt')
#        training_set = self.load('trainQT')
#        dev_set = self.load('devQT')
        questions = list()
        good_answers = list()

        d_questions = []
        d_g_answers = []

        for q in training_set:
            questions += [q['question']] * len(q['good'])
            good_answers += [self.answers[i] for i in q['good']]

        questions = self.padq(questions)
        good_answers = self.pada(good_answers)

        for d in dev_set:
            d_questions += [d['question']] * len(d['good'])
            d_g_answers += [self.answers[i] for i in d['good']]

        d_questions = self.padq(d_questions)
        d_g_answers = self.pada(d_g_answers)

        val_loss = {'loss': 1., 'epoch': 0}

        for i in range(1, nb_epoch):
            # sample from all answers to get bad answers
            bad_answers = self.pada(random.sample(self.answers.values(), len(good_answers)))
            d_b_answers = self.pada(random.sample(self.answers.values(), len(d_g_answers)))
            fixed_bads = np.empty((0, self.conf.get('answer_len')), int)
            d_f_bads = np.empty((0, self.conf.get('answer_len')), int)


            for (gs, bs) in zip(good_answers, bad_answers):
                if not (gs == bs).all():
                    fixed_bads = np.append(fixed_bads, [bs], axis=0)
                else:
                    fixed_bads = np.append(fixed_bads, self.pada(random.sample(self.answers.values(), 1)), axis=0)


            for (gs, bs) in zip(d_g_answers, d_b_answers):
                if not (gs == bs).all():
                    d_f_bads = np.append(d_f_bads, [bs], axis=0)
                else:
                    d_f_bads = np.append(d_f_bads, self.pada(random.sample(self.answers.values(), 1)), axis=0)


            print('Epoch %d :: ' % i, end='')
            bad_answers = fixed_bads
            d_b_answers = d_f_bads
            self.print_time()

            for q, g, b in itertools.izip(questions, good_answers, bad_answers):
                debugTrainFile.write("q:" + stripAccent(" ".join(self.revert(q))) + "\n")
                debugTrainFile.write("g:" + stripAccent(" ".join(self.revert(g))) + "\n")
                debugTrainFile.write("b:" + stripAccent(" ".join(self.revert(b))) + "\n")


            d_y = np.zeros(shape=(d_questions.shape[0],))

            hist = model.fit([questions, good_answers, bad_answers], nb_epoch=1, batch_size=batch_size,shuffle=False,
                             validation_data=([d_questions, d_g_answers, d_b_answers], d_y))

            if hist.history['val_loss'][0] < val_loss['loss']:
                val_loss = {'loss': hist.history['val_loss'][0], 'epoch': i}
                epoch_count = 0
                self.save_epoch(model, i)
            else:
                epoch_count += 1
                if epoch_count >= 200: break
            print('Best: Loss = {}, Epoch = {}'.format(val_loss['loss'], val_loss['epoch']))

            if save_every is not None and i % save_every == 0:
                self.save_epoch(model, i)
        debugTrainFile.close()
        return val_loss

    ##### Evaluation #####

    def prog_bar(self, so_far, total, n_bars=20):
        n_complete = int(so_far * n_bars / total)
        if n_complete >= n_bars - 1:
            print('\r[' + '=' * n_bars + ']', end='')
        else:
            s = '\r[' + '=' * (n_complete - 1) + '>' + '.' * (n_bars - n_complete) + ']'
            print(s, end='')

    def eval_sets(self):
        if self._eval_sets is None:
            #            self._eval_sets = dict([(s, self.load(s)) for s in ['woHfValidV2']])
            #            full_set = self.load('testV2WoHf')
#            full_set = self.load('devQTCt')
#            full_set = self.load('devQTCt')
#            full_set = self.load('devQTd')
#            full_set = self.load('devQTd_when')
#            testQTd_when
#            test_set = full_set
            #            self._eval_sets = dict([(s, self.load(s)) for s in test_set])
            #            self._eval_sets = dict([(test_set, pickle.load(test_set))])
            self._eval_sets = dict([(s, self.load(s)) for s in ['devG', 'testG']])
        return self._eval_sets

    def averPrec(self, indices, r, goodInd):
        indRankDict = {}
        ap = 0
        p = 0
        for i, r in zip(indices, r):
            indRankDict[i] = r
        for j, si in enumerate(sorted(indRankDict, key=lambda x: indRankDict[x], reverse=True)):
            if si in goodInd:
                p += 1
                ap += float(p) / (j + 1)
        if len(goodInd) > 0:
            ap = ap / len(goodInd)
        else:
            ap = 0
        return ap


    def get_mrr(self, model, evaluate_all=False):
        top1s = list()
        mrrs = list()
        maps = list()
        debugPredictFile = open("debugPredictWikiCt", "w")
        for name, data in self.eval_sets().items():
            if evaluate_all:
                self.print_time()
                print('----- %s -----' % name)

            random.shuffle(data)

            if not evaluate_all and 'n_eval' in self.params:
                data = data[:self.params['n_eval']]

            c_1, c_2, c_3 = 0, 0, 0
            c_2_who = 0
            len_who = 0
            c_2_where = 0
            len_where = 0
            c_2_when = 0
            len_when = 0
            c_2_oth = 0
            len_oth = 0

            for i, d in enumerate(data):
                if evaluate_all:
                    self.prog_bar(i, len(data))
                indices = d['good'] + d['bad']
                #                indices = d['good'] + d['bad']
                #                try:
                answers = self.pada([self.answers[i] for i in indices])
                question = self.padq([d['question']] * len(indices))
                #                except KeyError:
                #                    print (d)

                n_good = len(d['good'])
                #                for q, a in itertools.izip(question, answers):
                #                print ("q\n")
                #                print (q)
                #                    debugPredictFile.write("q:" + " ".join(self.revert(q)) + "\n")
                #                    debugPredictFile.write("a:" + " ".join(self.revert(a)) + "\n")


                sims = model.predict([question, answers])
                r = rankdata(sims, method='max')
                #print("r")
                #print(r)
                max_r = np.argmax(sims)
                max_n = np.argmax(sims[:n_good])
                #print("maxr maxn and sims[n_good]")
                #print(max_r)
                #print(max_n)
                #print(sims[:n_good])
                #print("sims")
                #print(sims)
                #print(' '.join(self.revert(d['question'])))

                #print(' '.join(self.revert(self.answers[indices[max_r]])))
                #print(' '.join(self.revert(self.answers[indices[max_n]])))
                c_1 += 1 if max_r == max_n else 0
                c_2 += 1 / float(r[max_r] - r[max_n] + 1)
                c_3 += self.averPrec(indices, r, d['good'])

                if self.revert(d['question'])[0] == "who":
                    c_2_who += 1 / float(r[max_r] - r[max_n] + 1)
                    len_who += 1
                elif self.revert(d['question'])[0] == "where":
                    c_2_where += 1 / float(r[max_r] - r[max_n] + 1)
                    len_where += 1
                elif self.revert(d['question'])[0] == "when":
                    c_2_when += 1 / float(r[max_r] - r[max_n] + 1)
                    len_when += 1
                else:
                    c_2_oth += 1 / float(r[max_r] - r[max_n] + 1)
                    len_oth += 1

            top1 = c_1 / float(len(data))
            #            top1_who = c_1_who / float(len_who)
            #            top1_where = c_1_where / float(len_where)
            #            top1_when = c_1_when / float(len_when)
            mrr = c_2 / float(len(data))
            map = c_3 / float(len(data))
            if not len_who == 0: mrr_who = c_2_who / float(len_who)
            if not len_where == 0: mrr_where = c_2_where / float(len_where)
            if not len_when == 0: mrr_when = c_2_when / float(len_when)
            if not len_oth == 0: mrr_oth = c_2_oth / float(len_oth)

            #            del data

            if evaluate_all:
                print('Top-1 Precision: %f' % top1)
                print('Length: %f' % len(data))
                #                print('Top-1 Precision (who): %f' % top1_who)
                #                print('Length (who): %f'https://github.com/mynlp/pairwiseFork/blob/master/trainQA.lua % len_who)
                #                print('who/whole: %f' % len_who/len(data))
                #                print('Top-1 Precision (where): %f' % top1_where)
                #                print('Length (where): %f' % len_where)
                #                print('where/whole: %f' % len_where/len(data))
                #                print('Top-1 Precision (when): %f' % top1_when)
                #                print('Length (when): %f' % len_when)
                print('MRR: %f' % mrr)
                print('MAP: %f' % map)
                if not len_who == 0:
                    print('MRR (who): %f' % mrr_who)
                    print('Length (who): %f' % len_who)
                if not len_where == 0:
                    print('MRR (where): %f' % mrr_where)
                    print('Length (where): %f' % len_where)
                if not len_when == 0:
                    print('MRR (when): %f' % mrr_when)
                    print('Length (when): %f' % len_when)
                if not len_oth == 0:
                    print('MRR (oth): %f' % mrr_oth)
                    print('Length (other): %f' % len_oth)
            del data
            top1s.append(top1)
            mrrs.append(mrr)
            maps.append(map)




        # rerun the evaluation if above some threshold

        if not evaluate_all:
            print('Top-1 Precision: {}'.format(top1s))
            print('MRR: {}'.format(mrrs))
            print('MAP: {}'.format(maps))
            evaluate_all_threshold = self.params.get('evaluate_all_threshold', dict())
            evaluate_mode = evaluate_all_threshold.get('mode', 'all')
            mrr_theshold = evaluate_all_threshold.get('mrr', 1)
            map_threshold = evaluate_all_threshold.get('map', 1)
            top1_threshold = evaluate_all_threshold.get('top1', 1)

            if evaluate_mode == 'any':
                evaluate_all = evaluate_all or any([x >= top1_threshold for x in top1s])
                evaluate_all = evaluate_all or any([x >= mrr_theshold for x in mrrs])
                evaluate_all = evaluate_all or any([x >= map_threshold for x in maps])
            else:
                evaluate_all = evaluate_all or all([x >= top1_threshold for x in top1s])
                evaluate_all = evaluate_all or all([x >= mrr_theshold for x in mrrs])
                evaluate_all = evaluate_all or all([x >= map_threshold for x in maps])

            if evaluate_all:
                return self.get_mrr(model, evaluate_all=True)
        debugPredictFile.close()
        return top1s, mrrs, maps


if __name__ == '__main__':
    import numpy as np

    conf = {
        'question_len': 50,
        'answer_len': 100,
        'n_words': 2196018, #for wordG
#        'n_words': 174015,  # len(vocabulary) + 1 for wikiQTd
#        'n_words': 30555,  # len(vocabulary) + 1 for wikiQTCt
#        'n_words': 31940,  # len(vocabulary) + 1 for wikiQT
        'margin': 0.2,

        'training_params': {
            'save_every': None,
            'batch_size': 100,
            'nb_epoch': 2001,
#for speed up debugging
#            'nb_epoch': 201,
            'validation_split': 0.1,
            'optimizer': Adam(clipnorm=1e-2),
        },

        'model_params': {
#            'n_embed_dims': 100,
            'n_embed_dims': 300,
            'n_hidden': 200,

            # convolution
            'nb_filters': 1000,  # * 4
            'conv_activation': 'tanh',

            # recurrent
            'n_lstm_dims': 141,  # * 2

            #            'initial_embed_weights': np.load('word2vec_100_dim.embeddings'),
            'similarity_dropout': 0.5,
        },
        'initial_embed_weights': 'glove.embedding.added0',
#        'initial_embed_weights': 'wikiQTCt_100_dim.embeddings',
#        'initial_embed_weights': 'wikiQT_100_dim.embeddings',

        'similarity': {
            'mode': 'cosine',
            'gamma': 1,
            'c': 1,
            'd': 2,
        }
    }

    evaluator = Evaluator(conf)

    ##### Define model ######
    model = AttentionModel(conf)
    optimizer = conf.get('training_params', dict()).get('optimizer', 'rmsprop')
    model.compile(optimizer=optimizer)

    # save embedding layer
    # evaluator.load_epoch(model, 7)
    # embedding_layer = model.prediction_model.layers[2].layers[2]
    # weights = embedding_layer.get_weights()[0]
    # np.save(open('models/embedding_1000_dim.h5', 'wb'), weights)

    # train the model
    # evaluator.load_epoch(model, 6)
    best_loss = evaluator.train(model)

    # evaluate mrr for a particular epoch
    evaluator.load_epoch(model, best_loss['epoch'])
    # evaluator.load_epoch(model, 31)
    evaluator.get_mrr(model, evaluate_all=True)
    # for epoch in range(1, 100):
    #     print('Epoch %d' % epoch)
    #     evaluator.load_epoch(model, epoch)
    #     evaluator.get_mrr(model, evaluate_all=True)

