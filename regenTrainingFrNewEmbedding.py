
from __future__ import print_function

import codecs
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import unicodedata

def stripAccent(text):

   text = unicode(text).decode('utf-8')

   return str(''.join(char for char in unicodedata.normalize('NFKD', text) if unicodedata.category(char) != 'Mn'))


import numpy


import pickle







 ##### Converting / reverting #####

def convert(rvocab, words):
    if type(words) == str:
        words = words.strip().lower().split(' ')
    return [rvocab.get(w, 0) for w in words]


def revert(vocab, indices):
    return [vocab.get(i, 'X') for i in indices]

    ##### Padding #####


def regen(dataList, oldVocab, newRevVocab):
    newDataList = []
    for line in dataList:
        oldInd = line["question"]
        newLine = {}
#        newInd = convert(newRevVocab, u" ".join(revert(oldVocab, oldInd)).encode('utf-8'))
        newInd = convert(newRevVocab, u" ".join(revert(oldVocab, oldInd)))
        newLine["question_id"] = line["question_id"]
        newLine["question"] = newInd
        newLine["good"] = line["good"]
        newLine["bad"] = line["bad"]
        newDataList.append(newLine)
    return newDataList


    ##### Evaluation #####



if __name__ == '__main__':
    oldTrain = pickle.load(open("/media/wailoktam/wikiqa/trainQTCt","rb"))
    oldDev = pickle.load(open("/media/wailoktam/wikiqa/devQTCt","rb"))
    oldTest = pickle.load(open("/media/wailoktam/wikiqa/testQTCt","rb"))
    oldVocab = pickle.load(open("/media/wailoktam/wikiqa/wordQTCt","rb"))
    oldAns = pickle.load(open("/media/wailoktam/wikiqa/answerQTCt","rb"))
    oldRevVocab = pickle.load(open("/media/wailoktam/wikiqa/revWordQTCt","rb"))
    debugRegen = open("debugRegen","w")
    newVocab = {}
    newRevVocab = {}
    newAns = []
    newVocabCount = 1
#    newVocabFile = "wordQTd"

    newEmbedFile = codecs.open("deps.words","r","utf-8")


    for line in newEmbedFile:
        wdAndVec = line.split(" ")
        word = wdAndVec[0]
        vec = numpy.array(wdAndVec[1:])
        print ("shape\n")
        print (vec.shape)

        newVocab[newVocabCount] = stripAccent(word.lower())
        newRevVocab[stripAccent(word.lower())] = newVocabCount

        if newVocabCount == 1:
            newWeights = numpy.array([vec])
            print("init newWeights\n")
            print(newWeights.shape)
        else:
            newWeights = numpy.append(newWeights,[vec],axis=0)
            print("newWeights\n")
            print(newWeights.shape)
        newVocabCount += 1

    for line in oldAns:
        oldInd = line["text"]
        newLine = {}
        debugRegen.write("oldAns\n")
#        debugRegen.write(u" ".join(revert(oldVocab,oldInd)).encode('utf-8'))
        debugRegen.write(u" ".join(revert(oldVocab,oldInd)))
        debugRegen.write("\n")
        newInd = convert(newRevVocab, revert(oldVocab, oldInd))
#        newInd = convert(newRevVocab, u" ".join(revert(oldVocab,oldInd)).encode('utf-8'))
        debugRegen.write("newAns\n")
#        debugRegen.write(u" ".join(revert(newVocab, newInd)).encode('utf-8'))
        debugRegen.write(u" ".join(revert(newVocab, newInd)))
        debugRegen.write("\n")
        newLine["id"] = line["id"]
        newLine["text"] = newInd
        newAns.append(newLine)



    newTrain = regen(oldTrain,oldVocab,newRevVocab)
    newDev = regen(oldDev, oldVocab, newRevVocab)
    newTest = regen(oldTest, oldVocab, newRevVocab)

    newRevVocabFile = open("/media/wailoktam/wikiqa/revWordQTCtd","wb")
    pickle.dump(newRevVocab, newRevVocabFile)
    newRevVocabFile.close()

    newVocabFile = open("/media/wailoktam/wikiqa/wordQTCtd", "wb")
    pickle.dump(newVocab, newVocabFile)
    newVocabFile.close()

    newTrainFile = open("/media/wailoktam/wikiqa/trainQTCtd", "wb")
    pickle.dump(newTrain, newTrainFile)
    newTrainFile.close()

    newDevFile = open("/media/wailoktam/wikiqa/devQTCtd", "wb")
    pickle.dump(newDev, newDevFile)
    newDevFile.close()

    newTestFile = open("/media/wailoktam/wikiqa/testQTCtd", "wb")
    pickle.dump(newTest, newTestFile)
    newTestFile.close()

    newAnsFile = open("/media/wailoktam/wikiqa/answerQTCtd", "wb")
    pickle.dump(newAns, newAnsFile)
    newAnsFile.close()

    newWeightsFile = codecs.open("wikiQTd_300_dim.embeddings", "wb", "utf-8")
    numpy.save(newWeightsFile, newWeights)
    newWeightsFile.close()

    debugRegen.close()
