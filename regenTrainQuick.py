
from __future__ import print_function



import numpy


import pickle








    ##### Converting / reverting #####

def convert(rvocab, words):
    if type(words) == str:
        words = words.strip().lower().split(' ')
    return [rvocab.get(w, 0) for w in words]


def revert(vocab, indices):
    return [vocab.get(i, 'X') for i in indices]

    ##### Padding #####


def regen(dataList, oldVocab, newRevVocab):
    newDataList = []
    for line in dataList:
        oldInd = line["question"]
        newLine = {}
        newInd = convert(newRevVocab, u" ".join(revert(oldVocab, oldInd)).encode('utf-8'))
        newLine["question_id"] = line["question_id"]
        newLine["question"] = newInd
        newLine["good"] = line["good"]
        newLine["bad"] = line["bad"]
        newDataList.append(newLine)
    return newDataList


    ##### Evaluation #####



if __name__ == '__main__':
    oldTrain = pickle.load(open("/media/wailoktam/wikiqa/trainQT_when","rb"))
    oldDev = pickle.load(open("/media/wailoktam/wikiqa/devQT_when","rb"))
    oldTest = pickle.load(open("/media/wailoktam/wikiqa/testQT_when","rb"))
    oldVocab = pickle.load(open("/media/wailoktam/wikiqa/wordQT","rb"))
    oldRevVocab = pickle.load(open("/media/wailoktam/wikiqa/revWordQT","rb"))
    newVocab = pickle.load(open("/media/wailoktam/wikiqa/wordQTd", "rb"))
    newRevVocab = pickle.load(open("/media/wailoktam/wikiqa/revWordQTd", "rb"))
    debugRegen = open("debugRegen","w")

#    newAns = []
#    newVocabCount = 1
#    newVocabFile = "wordQTd"
#    newEmbedFile = open("deps.words","r")







    newTrain = regen(oldTrain,oldVocab,newRevVocab)
    newDev = regen(oldDev, oldVocab, newRevVocab)
    newTest = regen(oldTest, oldVocab, newRevVocab)



    newTrainFile = open("/media/wailoktam/wikiqa/trainQTd_when", "wb")
    pickle.dump(newTrain, newTrainFile)
    newTrainFile.close()

    newDevFile = open("/media/wailoktam/wikiqa/devQTd_when", "wb")
    pickle.dump(newDev, newDevFile)
    newDevFile.close()

    newTestFile = open("/media/wailoktam/wikiqa/testQTd_when", "wb")
    pickle.dump(newTest, newTestFile)
    newTestFile.close()

    debugRegen.close()
